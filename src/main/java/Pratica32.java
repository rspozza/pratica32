/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *\[ densidade = \frac{1}{\sqrt{2\pi}\sigma} e^{-\frac{1}{2}(\frac{x-\mu}{\sigma})^2} \]
 *
 * @author pozza
 */
public class Pratica32 {

    public static double densidade(double x, double media, double desvio) {
        double d1, d2, d;
        d1 = 1 / Math.sqrt((2 / Math.PI) * desvio);
        d2 = -1 * (Math.pow(x - media, 2) / (2 * Math.pow(desvio, 2)));
        d = Math.pow(d1, d2);
        return d;
    }
    
    public static void main(String[] args) {
        System.out.println(" D = "+densidade(-1, 67, 3));
    }

}
